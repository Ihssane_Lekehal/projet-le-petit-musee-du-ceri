package l3ceri.omarserrar.museum;

import android.os.AsyncTask;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class CeriMuseumAPI extends AsyncTask<ObjetMusee, String, ArrayList<ObjetMusee>> {
    AppCompatActivity activity = null;
    CeriMuseumAPI(){
    }
    CeriMuseumAPI(AppCompatActivity activity){
        this.activity = activity;
    }

    @Override
    protected ArrayList<ObjetMusee> doInBackground(ObjetMusee... teams) {
        ArrayList<ObjetMusee> catalogue = new ArrayList<>();

        try {
            catalogue = JSONResponseHandlerMuseum.readCatalog();
            for(ObjetMusee objetMusee: catalogue){
                MuseeDBHelper.objets.put(objetMusee.getApiID(), objetMusee);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return catalogue;
    }
    @Override
    protected void onPostExecute(ArrayList<ObjetMusee> result) {
        if(activity instanceof  CatalogueActivity){
            ((CatalogueActivity)activity).actualiser();
        }
        if(activity instanceof CategoryList){
            ((CategoryList)activity).actualiser();
        }
    }
}
