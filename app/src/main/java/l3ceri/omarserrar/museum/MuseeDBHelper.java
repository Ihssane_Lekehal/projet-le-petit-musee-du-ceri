package l3ceri.omarserrar.museum;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class MuseeDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static HashMap<String, ObjetMusee> objets= new HashMap<>();
    public static HashMap<String, Categorie> categories = new HashMap<>();

    public static final String DATABASE_NAME = "musee.db";

    public static final String TABLE_NAME_OBJET = "objet";
    public static final String TABLE_NAME_CATEGORIE = "categorie";
    public static final String TABLE_NAME_PICTURES = "picture";
    public static final String TABLE_NAME_OBJET_TIME_FRAME = "objet_time_frame";
    public static final String TABLE_NAME_OBJET_TECHNICAL_DETAIL = "objet_technical_detail";
    public static final String TABLE_NAME_PICTURES_DATA = "picture_data";
    public static final String OBJET_ID = "ID";
    public static final String OBJET_API_ID = "API_ID";
    public static final String OBJET_NAME = "NAME";
    public static final String OBJET_DESCRIPTION = "DESCRIPTION";
    public static final String OBJET_YEAR = "YEAR";
    public static final String OBJET_BRAND = "BRAND";
    public static final String OBJET_WORKING = "WORKING";

    public static final String TECHNICAL_DETAIL = "TECHNICAL_DETAIL";
    public static  final String OBJ_ID = "OBJ_ID";
    public static  final  String TIME_FRAME = "TIME_FRAME";
    public static  final  String COMMENT = "COMMENT";
    public static  final  String CAT_ID = "CAT_ID";
    public  static final String CAT_NAME = "CAT_NAME";
    public static MuseeDBHelper dbHelper = null;
    public MuseeDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        dbHelper = this;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_OBJET_TABLE= "CREATE TABLE " + TABLE_NAME_OBJET +
                " ( API_ID STRING PRIMARY KEY," +
                "NAME TEXT," +
                "DESCRIPTION TEXT," +
                "YEAR INTEGER," +
                "BRAND TEXT," +
                "WORKING INTEGER);";
        final String SQL_CREATE_OBJET_CATEGORIE= "CREATE TABLE " + TABLE_NAME_CATEGORIE +
                " ( ID INTEGER PRIMARY KEY," +
                "CAT_NAME TEXT);";
        final String SQL_CREATE_OBJET_CATEGORIE_OBJET= "CREATE TABLE CATEGORIE_OBJET"+
                " ( CAT_ID INTEGER," +
                "OBJ_ID STRING," +
                "PRIMARY KEY(CAT_ID, OBJ_ID),"+
                " FOREIGN KEY (OBJ_ID) REFERENCES API_ID("+TABLE_NAME_OBJET+"),"+
                " FOREIGN KEY (CAT_ID) REFERENCES ID("+TABLE_NAME_CATEGORIE+"));";

        final String SQL_CREATE_OBJET_PICTURES= "CREATE TABLE " + TABLE_NAME_PICTURES +
                " ( ID TEXT PRIMARY KEY," +
                "COMMENT TEXT," +
                "OBJ_ID STRING," +
                "data blob,"+
                " FOREIGN KEY (OBJ_ID) REFERENCES API_ID("+TABLE_NAME_OBJET+"));";

        final String SQL_CREATE_TIME_FRAME = "CREATE TABLE " + TABLE_NAME_OBJET_TIME_FRAME +
                " ( ID INTEGER PRIMARY KEY," +
                "TIME_FRAME INTEGER," +
                "OBJ_ID STRING,"+
                " FOREIGN KEY (OBJ_ID) REFERENCES API_ID("+TABLE_NAME_OBJET+"));";

        final String SQL_TECHNINCAL_DETAIL = "CREATE TABLE " + TABLE_NAME_OBJET_TECHNICAL_DETAIL +
                " ( "+
                "TECHNICAL_DETAIL STRING," +
                "OBJ_ID INTEGER," +
                "PRIMARY KEY(OBJ_ID, TECHNICAL_DETAIL)," +
                " FOREIGN KEY (OBJ_ID) REFERENCES API_ID("+TABLE_NAME_OBJET+"));";
        final String SQL_PICTURES_DATA = "CREATE TABLE " + TABLE_NAME_PICTURES_DATA +
                " ( "+
                "url STRING PRIMARY KEY," +
                "data blob);";
        db.execSQL(SQL_CREATE_OBJET_TABLE);
        db.execSQL(SQL_CREATE_OBJET_CATEGORIE);
        db.execSQL(SQL_CREATE_OBJET_CATEGORIE_OBJET);
        db.execSQL(SQL_CREATE_OBJET_PICTURES);
        db.execSQL(SQL_CREATE_TIME_FRAME);
        db.execSQL(SQL_TECHNINCAL_DETAIL);
        db.execSQL(SQL_PICTURES_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public boolean addObjet(ObjetMusee objet){
        SQLiteDatabase db = this.getWritableDatabase();
        Integer workingVal = null;
        if(objet.isWorking()!=null){
            workingVal = objet.isWorking().booleanValue() ? 1:0;
        }
        ContentValues objetValues = new ContentValues();
        objetValues.put(OBJET_API_ID, objet.getApiID());
        objetValues.put(OBJET_BRAND, objet.getBrand());
        objetValues.put(OBJET_DESCRIPTION, objet.getDescription());
        objetValues.put(OBJET_NAME, objet.getName());
        objetValues.put(OBJET_YEAR, objet.getYear());
        objetValues.put(OBJET_WORKING, workingVal);
        for(String technicalDetail: objet.getTechnicalDetails()){
            ContentValues technicalDetailCV = new ContentValues();
            technicalDetailCV.put(TECHNICAL_DETAIL, technicalDetail);
            technicalDetailCV.put(OBJ_ID, objet.getApiID());
            long rowID = db.insertWithOnConflict(TABLE_NAME_OBJET_TECHNICAL_DETAIL, null, technicalDetailCV, CONFLICT_IGNORE);
        }
        for(Integer timeFrame: objet.getTimeFrame()){
            ContentValues timeFrameCV = new ContentValues();
            timeFrameCV.put(TIME_FRAME, timeFrame);
            timeFrameCV.put(OBJ_ID, objet.getApiID());
            long rowID = db.insertWithOnConflict(TABLE_NAME_OBJET_TIME_FRAME, null, timeFrameCV, CONFLICT_IGNORE);
        }
        for(String picture: objet.getPictures().keySet()){
            ContentValues pictureCV = new ContentValues();
            pictureCV.put("ID", picture);
            pictureCV.put("COMMENT", objet.getPictures().get(picture));
            pictureCV.put(OBJ_ID, objet.getApiID());
            long rowID = db.insertWithOnConflict(TABLE_NAME_PICTURES, null, pictureCV, CONFLICT_IGNORE);
        }
        for(Categorie categorie: objet.getCategories()){
            Cursor row = db.rawQuery("SELECT ID FROM "+TABLE_NAME_CATEGORIE+" WHERE CAT_NAME= ?",new String[]{categorie.getName()});
            int id = -1;
            if(row.moveToFirst()){
               id = row.getInt(0);
            }
            else{
                ContentValues categoryCV = new ContentValues();
                categoryCV.put("CAT_NAME", categorie.getName());
                id = (int)db.insertWithOnConflict(TABLE_NAME_CATEGORIE, null, categoryCV, CONFLICT_IGNORE);

            }
            ContentValues categoryObjetCV = new ContentValues();
            categoryObjetCV.put("CAT_ID", id);
            categoryObjetCV.put("OBJ_ID", objet.getApiID());
            db.insertWithOnConflict("CATEGORIE_OBJET", null, categoryObjetCV, CONFLICT_IGNORE);
        }
        long rowID = db.insertWithOnConflict(TABLE_NAME_OBJET, null, objetValues, CONFLICT_IGNORE);
        return true;
    }
    public Cursor fetchAll(String table, String orderBy) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(table, null,
                null, null, null, null, orderBy, null);

        Log.d("h", "call fetchAllTeams()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
    public long addPicture(Bitmap bitmap, String url, Bitmap.CompressFormat compressFormat){
        SQLiteDatabase db = this.getWritableDatabase();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, 70, stream);
        byte[] data =stream.toByteArray();
        ContentValues cv = new ContentValues();
        cv.put("data", data);
        cv.put("url", url);
        long i = db.insert(TABLE_NAME_PICTURES_DATA,null,cv);
        db.close();
        return i;
    }
    public Bitmap getPic(String url){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT data FROM "+TABLE_NAME_PICTURES_DATA+" WHERE url = '"+url+"'", null);
        c.moveToFirst();
        db.close();
        try{
            byte[] data = c.getBlob(0);

            return BitmapFactory.decodeByteArray(data, 0, data.length);
        }
        catch (Exception e){
            return null;
        }
    }
    public HashMap<String, ObjetMusee> getAllObjetList(){

        HashMap<String, ObjetMusee> allObjets = new HashMap<>();
        Cursor c = fetchAll(TABLE_NAME_OBJET, null);
        if(c.moveToFirst())
        while(!c.isAfterLast()){
            ObjetMusee objetMusee = cursorToObjet(c,null);
            allObjets.put(objetMusee.getApiID(), objetMusee);
            c.moveToNext();
        }
        return  allObjets;
    }
    public ObjetMusee cursorToObjet(Cursor cursor, SQLiteDatabase db){
        boolean localDb = false;
        if(db==null){
            localDb=true;
            db = this.getReadableDatabase();
        }
        ObjetMusee objetMusee = new ObjetMusee(cursor.getString(cursor.getColumnIndex(OBJET_API_ID)));
        if(cursor.getType(cursor.getColumnIndex(OBJET_BRAND))!= Cursor.FIELD_TYPE_NULL)
            objetMusee.setBrand(cursor.getString(cursor.getColumnIndex(OBJET_BRAND)));
        if(cursor.getType(cursor.getColumnIndex(OBJET_YEAR))!= Cursor.FIELD_TYPE_NULL)
            objetMusee.setYear(cursor.getInt(cursor.getColumnIndex(OBJET_YEAR)));
        if(cursor.getType(cursor.getColumnIndex(OBJET_DESCRIPTION))!= Cursor.FIELD_TYPE_NULL)
            objetMusee.setDescription(cursor.getString(cursor.getColumnIndex(OBJET_DESCRIPTION)));
        if(cursor.getType(cursor.getColumnIndex(OBJET_NAME))!= Cursor.FIELD_TYPE_NULL)
            objetMusee.setName(cursor.getString(cursor.getColumnIndex(OBJET_NAME)));
        if(cursor.getType(cursor.getColumnIndex(OBJET_WORKING))!= Cursor.FIELD_TYPE_NULL){
            if(cursor.getInt(cursor.getColumnIndex(OBJET_WORKING))==0){
                objetMusee.setWorking(true);
            }
            else objetMusee.setWorking(false);
        }
        Cursor categoriesCursor = db.rawQuery("SELECT CAT_NAME FROM CATEGORIE_OBJET inner join categorie on id = cat_id WHERE obj_id = ?", new String[]{objetMusee.getApiID()});
        categoriesCursor.moveToFirst();
        while(!categoriesCursor.isAfterLast()){
            objetMusee.addCategorie(categoriesCursor.getString(0));
            categoriesCursor.moveToNext();
        }
        Cursor technicalDetailsCursor = db.rawQuery("SELECT TECHNICAL_DETAIL FROM "+TABLE_NAME_OBJET_TECHNICAL_DETAIL+" WHERE obj_id = ?", new String[]{objetMusee.getApiID()});
        technicalDetailsCursor.moveToFirst();
        while(!technicalDetailsCursor.isAfterLast()){
            objetMusee.getTechnicalDetails().add(technicalDetailsCursor.getString(0));
            technicalDetailsCursor.moveToNext();
        }
        Cursor timeFrameCursor = db.rawQuery("SELECT TIME_FRAME FROM "+TABLE_NAME_OBJET_TIME_FRAME+" WHERE obj_id = ?", new String[]{objetMusee.getApiID()});
        timeFrameCursor.moveToFirst();
        while(!timeFrameCursor.isAfterLast()){
            objetMusee.getTimeFrame().add(timeFrameCursor.getInt(0));
            timeFrameCursor.moveToNext();
        }
        Cursor pictureCursor = db.rawQuery("SELECT ID,COMMENT FROM "+TABLE_NAME_PICTURES+" WHERE obj_id = ?", new String[]{objetMusee.getApiID()});
        pictureCursor.moveToFirst();
        while(!pictureCursor.isAfterLast()){
            objetMusee.getPictures().put(pictureCursor.getString(0),pictureCursor.getString(1));
            pictureCursor.moveToNext();
        }
        return objetMusee;
    }

}
