package l3ceri.omarserrar.museum;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ObjetMusee implements Serializable {
    private String apiID;
    private String name;
    private String description;
    private String brand;

    private int year;

    private Boolean working = null;

    private ArrayList<Integer> timeFrame;
    private ArrayList<Categorie> categories;
    private ArrayList<String> technicalDetails;

    private HashMap<String, String> pictures;



    public ObjetMusee(String apiID) {
        this.apiID = apiID;
        technicalDetails = new ArrayList<>();
        categories = new ArrayList<>();
        timeFrame = new ArrayList<>();
        pictures = new HashMap<>();
    }



    public HashMap<String, String> getPictures() {
        return pictures;
    }


    public String getApiID() {
        return apiID;
    }

    public void setApiID(String apiID) {
        this.apiID = apiID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public ArrayList<String> getTechnicalDetails() {
        return technicalDetails;
    }


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public ArrayList<Integer> getTimeFrame() {
        return timeFrame;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    public Categorie addCategorie(String categorieName){
        Categorie categorie = null;
        if(MuseeDBHelper.categories.containsKey(categorieName)){
            categorie = MuseeDBHelper.categories.get(categorieName);
        }

        else{
            categorie = new Categorie(categorieName);
            MuseeDBHelper.categories.put(categorieName, categorie);
        }
        getCategories().add(categorie);
        return categorie;
    }
    public ArrayList<Categorie> getCategories() {
        return categories;
    }
    public String toString(){
        return name+" "+year+" "+brand;
    }
    public URL getThumbNail(){
        try {
            return new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+apiID+"/thumbnail");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public boolean containtValue(String query){
        query = query.toLowerCase();
        if(new String(year+"").toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(name.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(apiID.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(description != null && description.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        if(brand != null && brand.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        for(Integer year: timeFrame){
            if(new String(year+"").toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        }
        for(String technicalDetail: technicalDetails){
            if(technicalDetail.toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        }
        for(Categorie categorie: categories){
            if(categorie.getName().toLowerCase().replace('é','e').replace('è','e').replace('ô','o').replace('ê','e').contains(query)) return true;
        }

        return false;
    }

/*
    @Override
    public int describeContents() {
        return 0;
    }

    public ObjetMusee(Parcel parcel){
        apiID = parcel.readString();
        name = parcel.readString();
        working = (Boolean)parcel.readValue(Boolean.class.getClassLoader());
        parcel.readStringList(technicalDetails);
        parcel.readMap(pictures, HashMap.class.getClassLoader());
        year = parcel.readInt();
        parcel.readList(timeFrame, ArrayList.class.getClassLoader());
        description = parcel.readString();
        brand = parcel.readString();
        parcel.readList(categories, ArrayList.class.getClassLoader());
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(apiID);
        dest.writeString(name);
        dest.writeValue(working);
        dest.writeStringList(technicalDetails);
        dest.writeMap(pictures);
        dest.writeInt(year);
        dest.writeList(timeFrame);
        dest.writeString(description);
        dest.writeString(brand);
        dest.writeList(categories);
    }
    public static final Creator<ObjetMusee> CREATOR = new Creator<ObjetMusee>()
    {
        @Override
        public ObjetMusee createFromParcel(Parcel source)
        {
            return new ObjetMusee(source);
        }

        @Override
        public ObjetMusee[] newArray(int size)
        {
            return new ObjetMusee[size];
        }
    };*/
}
