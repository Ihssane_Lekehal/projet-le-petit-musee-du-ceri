package l3ceri.omarserrar.museum;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

class Categorie implements Serializable {
    private String name;
    public Categorie(Parcel parcel){
        name = parcel.readString();
    }
    public Categorie(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ObjetMusee> getAllObjet(){
        ArrayList<ObjetMusee> objetMusees = new ArrayList<>();
        for(ObjetMusee objetMusee: MuseeDBHelper.objets.values()){
            if(objetMusee.getCategories().contains(this)){
                objetMusees.add(objetMusee);
            }
        }
        return objetMusees;
    }
    public static Categorie[] getAllCategories(){
        Categorie[] categories = new Categorie[MuseeDBHelper.categories.size()];
        Iterator<Categorie> iter = MuseeDBHelper.categories.values().iterator();
        for(int i=0;i<categories.length;i++){
            categories[i] = iter.next();
        }
        return  categories;
    }
    public String toString(){
        return name;

    }
}
