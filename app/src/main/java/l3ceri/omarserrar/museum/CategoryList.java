package l3ceri.omarserrar.museum;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class CategoryList  extends AppCompatActivity {
    ListView categoryList;
    LinearLayout chargement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("Chargement "+(chargement==null));
        setContentView(R.layout.category_view);
        categoryList = findViewById(R.id.category_list);
        chargement = findViewById(R.id.chargementCategorie);
        actualiser();
        new MuseeDBHelper(this);
        if(MuseeDBHelper.objets.size()==0){

            chargement.setVisibility(View.VISIBLE);
            new CeriMuseumAPI(this).execute();
        }

    }
    public void actualiser(){
        categoryList.setAdapter(new ArrayAdapter<Categorie>(this, android.R.layout.simple_list_item_1, Categorie.getAllCategories()));
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Categorie categorie = (Categorie)parent.getItemAtPosition(position);
                Intent myIntent = new Intent(CategoryList.this, CatalogueActivity.class);
                myIntent.putExtra("CATEGORY",categorie.getName());
                startActivity(myIntent);
            }
        });
        chargement.setVisibility(View.GONE);
    }

}