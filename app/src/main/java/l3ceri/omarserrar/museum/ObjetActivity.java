package l3ceri.omarserrar.museum;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ObjetActivity extends AppCompatActivity {
    private TextView annee;
    private  TextView marque;
    private TextView technicalDetails;
    private  TextView timeFrame;
    private TextView descritpion;
    private TextView working;
    private TextView categories;
    private TextView titre;
    private TextView indiceText;
    private ImageView thumb;
    private ImageView album;
    private ObjetMusee objet;
    private Button suivant;
    private Button precedent;
    private ArrayList<Bitmap> pictures = new ArrayList<>();
    private int picIndice = 1;
    private TextView loadingState;
    private LinearLayout loading;
    private TextView commentaire;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.objet_view);
        titre = (TextView)findViewById(R.id.titre);
        marque = (TextView)findViewById(R.id.marque);
        annee = (TextView)findViewById(R.id.annee);
        technicalDetails = (TextView)findViewById(R.id.details);
        timeFrame = (TextView)findViewById(R.id.anneeUtilisation);
        descritpion = (TextView)findViewById(R.id.description);
        working = (TextView)findViewById(R.id.fonctionne);
        categories = (TextView)findViewById(R.id.categories);
        thumb = findViewById(R.id.thumb);
        album = findViewById(R.id.album);
        indiceText = findViewById(R.id.indice_photo);
        objet = (ObjetMusee) getIntent().getSerializableExtra("objet");
        annee.setText((objet.getYear()==0)?"Indéfinie":objet.getYear()+"");
        marque.setText(objet.getBrand());
        descritpion.setText(objet.getDescription());
        suivant = findViewById(R.id.suiv);
        precedent = findViewById(R.id.prec);
        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoSuivante();
            }
        });
        precedent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoPrecedente();
            }
        });
        titre.setText(objet.getName());
        commentaire = findViewById(R.id.commentaire);
        thumb.setImageDrawable(null);
        if(new DownloadImageTask(thumb).execute(objet.getThumbNail())==null)
            thumb.setImageResource(R.drawable.ic_launcher_background);
        if(objet.isWorking()!=null)
            working.setText((objet.isWorking())?"Oui":"Non");
        String technicalDetailsString = "";
        for(String technicalDetail: objet.getTechnicalDetails())
            technicalDetailsString += "-"+technicalDetail+"\n";
        if(!technicalDetailsString.isEmpty())
            technicalDetails.setText(technicalDetailsString);

        String timeFrameString = "";
        for(Integer year: objet.getTimeFrame())
            timeFrameString += "-"+year+"\n";
        if(!timeFrameString.isEmpty())
            timeFrame.setText(timeFrameString);

        String categoriesString = "";
        for(Categorie categorie: objet.getCategories())
            categoriesString += "-"+categorie.getName()+"\n";
        if(!categoriesString.isEmpty())
            categories.setText(categoriesString);
        if(objet.getPictures().size()>0){
            loading = findViewById(R.id.chargementPictures);
            loadingState = findViewById(R.id.pic_loading_text);
            loading.setVisibility(View.VISIBLE);
            int i = 0;
            loadingState.setText("Téléchargement des photos "+(pictures.size())+" sur "+objet.getPictures().size());
            for(String picID: objet.getPictures().keySet()){
                try {
                    URL url = new URL("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+objet.getApiID()+"/images/"+picID);
                    new PictureManager().execute(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            }
        }
        else{
            indiceText.setText("Aucune Photo");
        }

    }
    private void photoPrecedente(){
        if(pictures.size()==0) return;
        picIndice--;
        if(picIndice<1) picIndice = pictures.size();
        showPicture(picIndice);
    }
    private void photoSuivante(){
        if(pictures.size()==0) return;
        picIndice++;
        if(picIndice>pictures.size()) picIndice = 1;
        showPicture(picIndice);
    }
    private void showPicture(int indice){
        indiceText.setText(indice+"/"+pictures.size());
        album.setImageBitmap(pictures.get(indice-1));
        String comment = new ArrayList<String>(objet.getPictures().values()).get(indice-1);
        commentaire.setText(comment);
    }
    class PictureManager extends AsyncTask<URL, Void, Bitmap> {
        protected Bitmap doInBackground(URL... url) {
            Bitmap badgeBitmap = null;
            if(DownloadImageTask.picturesCache.containsKey(url[0])){
                return DownloadImageTask.picturesCache.get(url[0]);
            }
            Bitmap photo = CatalogueActivity.dbHelper.getPic(url[0].toString());
            if(photo!=null){
                System.out.println("Recuperation de "+url[0]+" de la bd");
                return photo;
            }
            try {
                InputStream inputStream = url[0].openStream();
                badgeBitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            DownloadImageTask.picturesCache.put(url[0], badgeBitmap);
            return badgeBitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if(result==null){
                loadingState.setText("Aucune connexion internet !");

                return;
            }
            pictures.add(result);
            loadingState.setText("Téléchargement des photos "+(pictures.size())+" sur "+objet.getPictures().size());
            if(pictures.size()==1){
                album.setImageBitmap(result);
                commentaire.setText(new ArrayList<String>(objet.getPictures().values()).get(0));
            }

            indiceText.setText((picIndice)+"/"+pictures.size());
            if(pictures.size() == objet.getPictures().size()){
                loading.setVisibility(View.GONE);
                int i=0;
                ArrayList<String> picsUrl = new ArrayList<>(objet.getPictures().keySet());
                for(Bitmap picture: pictures){
                    try{
                        String url = "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+objet.getApiID()+"/images/"+picsUrl.get(i++);
                        CatalogueActivity.dbHelper.addPicture(picture, url, Bitmap.CompressFormat.JPEG);
                    }
                    catch (Exception e){

                    }
                }
            }
        }

    }
}



