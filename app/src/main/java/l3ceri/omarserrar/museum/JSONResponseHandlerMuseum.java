package l3ceri.omarserrar.museum;


import android.util.JsonReader;
import android.util.Log;
import android.widget.Switch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class JSONResponseHandlerMuseum {
    private static String url = "https://demo-lia.univ-avignon.fr/cerimuseum/";
    private static final String TAG = JSONResponseHandlerMuseum.class.getSimpleName();


    private static String readJsonStream(URL url) throws IOException {
        System.out.println(url);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String body="";
        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            body += currentLine;
        }
        return body;
    }

    public static ArrayList<ObjetMusee> readCatalog() throws IOException {
        ArrayList<ObjetMusee> catalogue = new ArrayList<>();
        URL catalogApiURL = new URL(url+"collection");
        JSONObject catalogJson = null;
        try {
            catalogJson = new JSONObject(readJsonStream(catalogApiURL));
        Iterator<String> keys = catalogJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                if(catalogJson.get(key) instanceof  JSONObject){
                    ObjetMusee objetMusee = readObjetMusee(key, catalogJson.getJSONObject(key));
                    System.out.println(objetMusee);
                    catalogue.add(objetMusee);
                    MuseeDBHelper.dbHelper.addObjet(objetMusee);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return catalogue;
    }
    public static ObjetMusee readObjetMusee(String key, JSONObject objectJson) throws IOException {
        ObjetMusee objetMusee = new ObjetMusee(key);
        Iterator<String> objetKeys = objectJson.keys();
        while (objetKeys.hasNext()) {
            String Objetkey = objetKeys.next();
            try {
                switch (Objetkey) {
                    case "name":
                        objetMusee.setName(objectJson.getString(Objetkey));
                        break;
                    case "categories":
                        JSONArray categoriesArray = objectJson.getJSONArray(Objetkey);
                        for (int i = 0; i < categoriesArray.length(); i++){
                            objetMusee.addCategorie(categoriesArray.getString(i));
                        }

                        break;
                    case "technicalDetails":
                        JSONArray technicalDetailsArray = objectJson.getJSONArray(Objetkey);
                        for (int i = 0; i < technicalDetailsArray.length(); i++)
                            objetMusee.getTechnicalDetails().add(technicalDetailsArray.getString(i));
                        break;
                    case "timeFrame":
                        JSONArray timeFrame = objectJson.getJSONArray(Objetkey);
                        for (int i = 0; i < timeFrame.length(); i++)
                            objetMusee.getTimeFrame().add(timeFrame.getInt(i));
                        break;
                    case "description":
                        objetMusee.setDescription(objectJson.getString(Objetkey));
                        break;
                    case "year":
                        objetMusee.setYear(objectJson.getInt(Objetkey));
                        break;
                    case "brand":
                        objetMusee.setBrand(objectJson.getString(Objetkey));
                        break;
                    case "pictures":
                        JSONObject picturesArray = objectJson.getJSONObject(Objetkey);
                        Iterator<String> pictures = picturesArray.keys();
                        while(pictures.hasNext()){
                            String id = pictures.next();
                            String comment = picturesArray.getString(id);
                            objetMusee.getPictures().put(id,comment);
                        }
                        break;
                    case "working":
                        objetMusee.setWorking(objectJson.getBoolean(Objetkey));
                        break;
                    default:
                        System.out.println("Undefined Key " + Objetkey);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return objetMusee;
    }
    public static String nextString(JsonReader reader){
        try{
            return reader.nextString();
        }
        catch (Exception e){
            return null;
        }
    }
    public static String nextName(JsonReader reader){
        try{
            return reader.nextName();
        }
        catch (Exception e){
            return null;
        }
    }
    public static int nextInt(JsonReader reader){
        try{
            return reader.nextInt();
        }
        catch (Exception e){
            try {
                reader.nextNull();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return Integer.MIN_VALUE;
        }
    }
}
